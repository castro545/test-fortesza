Instrucciones de Instalación:

Tener la version de PHP 8.0.2 o superior.
Ejecutar el comando: composer install
Luego tendriamos que crear la base de datos y agregar el nombre de la misma, usuario y contraseña al archivo .env
Una vez tengamos conexion existosa,  debemos ejecutar el comando: php artisan migrate y luego ejecutar el comando: php artisan db:seed.Luego ejecutar el comando: php artisan serve y se ejecutara el proyecto por el puerto 8000 de localhost


Documentacion del API:
Se encuentra en la ruta: doc/index.html
NOTA: Este código solo abarca la solucion de los 3 puntos que se menciona en el siguiente enunciado "Imaginemos lo siguiente: Tenemos un sitio web llamado mi-sitio.demo en el cual cualquier usuario registrado
